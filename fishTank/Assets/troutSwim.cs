﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(ParticleSystem))]

public class troutSwim : MonoBehaviour {
	protected Rigidbody2D m_RigidBody2D;
	protected ParticleSystem m_particles;
	protected Canvas CanvasObject; // Assign in inspector
	protected ParticleSystem.MainModule m_ParticleMain;
	bool m_spawning = false;
	protected float speed = 5;
	private void Awake()
	{
		m_RigidBody2D = GetComponent<Rigidbody2D> ();

	}
	// Use this for initialization
	void Start () {

		CanvasObject = GetComponent<Canvas> ();
		m_particles = GetComponent<ParticleSystem> ();
		m_ParticleMain = GetComponent<ParticleSystem>().main;
		//m_ParticleMain.startColor = Color.blue; // <- or whatever color you want to assign
	}
		

	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		m_RigidBody2D.AddForce(new Vector2(moveHorizontal*speed, moveVertical*speed));

		bool brakes = Input.GetKeyDown (KeyCode.Space);

		if (brakes) {
			m_RigidBody2D.velocity = (new Vector2 (0, 0));
		}

		bool talk = Input.GetKeyDown (KeyCode.Tab);
		if (talk) {
			CanvasObject.enabled = !CanvasObject.enabled;
		}


		bool eggs = Input.GetKey (KeyCode.RightShift);
		if (eggs && !m_spawning) {
			m_particles.Play();
			m_spawning = true;
		} else if(!eggs && m_spawning) {
			print("stopping eggs");
				m_spawning = false;
				m_particles.Stop();
		}
	}



}
	


/*
 * 
 * using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamekit2D
{
	[RequireComponent(typeof(CharacterController2D))]
	[RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(Collider2D))]


	public class valPlayer : MonoBehaviour
	{

		static Collider2D[] s_ColliderCache = new Collider2D[16];
		protected Vector3 m_MoveVector;
		public Vector3 moveVector { get { return m_MoveVector; } }
		[Header("Movement")]
		public float speed = 2.0f;
		public float gravity = 10.0f;        

		protected SpriteRenderer m_SpriteRenderer;
		protected CharacterController2D m_CharacterController2D;
		protected Rigidbody2D m_RigidBody2D;
		protected Collider2D m_Collider;
		protected Animator m_Animator;
		protected ContactFilter2D m_Filter;
		protected Bounds m_LocalBounds;
		[Tooltip("If the sprite face left on the spritesheet, enable this. Otherwise, leave disabled")]
		public bool spriteFaceLeft = true;
		bool lastDirectionLeft=false;


		//as we flip the sprite instead of rotating/scaling the object, this give the forward vector according to the sprite orientation
		protected Vector2 m_SpriteForward;

		private void Awake()

		{   m_RigidBody2D = GetComponent<Rigidbody2D> ();
			m_CharacterController2D = GetComponent<CharacterController2D> ();
			m_Animator = GetComponent<Animator>();
			m_Collider = GetComponent<Collider2D>();
			m_LocalBounds = new Bounds();
			m_SpriteRenderer = GetComponent<SpriteRenderer>();

			int count = m_RigidBody2D.GetAttachedColliders(s_ColliderCache);
			for (int i = 0; i < count; ++i)
			{
				m_LocalBounds.Encapsulate(transform.InverseTransformBounds(s_ColliderCache[i].bounds));
			}

			m_SpriteForward = spriteFaceLeft ? Vector2.left : Vector2.right;

			//if (m_SpriteRenderer.flipX) m_SpriteForward = -m_SpriteForward;
			print (m_SpriteForward);
			print ("is the first forward");

		}

		private void Start()
		{
			SceneLinkedSMB<valPlayer>.Initialise(m_Animator, this);
			m_Filter = new ContactFilter2D();
			m_Filter.layerMask = m_CharacterController2D.groundedLayerMask;
			m_Filter.useLayerMask = true;
			m_Filter.useTriggers = false;
		}

		void FixedUpdate()
		{
			print (m_SpriteForward);
			print ("is the update forward");
			//Store the current horizontal input in the float moveHorizontal.
			float moveHorizontal = Input.GetAxis ("Horizontal");
			bool jumpInput = Input.GetKeyDown (KeyCode.Space);
			print(lastDirectionLeft);
			UpdateFacing(moveHorizontal);
			SetHorizontalSpeed (moveHorizontal);
			m_MoveVector.y = Mathf.Max(m_MoveVector.y - gravity * Time.deltaTime, - gravity);
			m_CharacterController2D.Move(m_MoveVector * Time.deltaTime);

		}

		public bool CheckForObstacle(float forwardDistance)
		{
			//we circle cast with a size sligly small than the collider height. That avoid to collide with very small bump on the ground

			Vector3 endPoint = m_Collider.bounds.center;
			endPoint.x = (m_SpriteForward.x * forwardDistance) + endPoint.x;
			Debug.DrawLine(m_Collider.bounds.center, endPoint);
			if (Physics2D.CircleCast(m_Collider.bounds.center, m_Collider.bounds.extents.y - 0.2f, m_SpriteForward, forwardDistance, m_Filter.layerMask.value))
			{
				return true;
			}


			Vector3 castingPosition = (Vector2)(transform.position + m_LocalBounds.center) + m_SpriteForward * m_LocalBounds.extents.x;
			Debug.DrawLine(castingPosition, castingPosition + Vector3.down * (m_LocalBounds.extents.y + 1f));

			if (!Physics2D.CircleCast(castingPosition, 0.1f, Vector2.down, m_LocalBounds.extents.y + 1f, m_CharacterController2D.groundedLayerMask.value))
			{
				return true;
			}

			return false;
		}

		public void SetFacingData(int facing)
		{
			if (facing == -1)
			{
				m_SpriteRenderer.flipX = !spriteFaceLeft;
				m_SpriteForward = spriteFaceLeft ? Vector2.right : Vector2.left;
			}
			else if (facing == 1)
			{
				m_SpriteRenderer.flipX = spriteFaceLeft;
				m_SpriteForward = spriteFaceLeft ? Vector2.left : Vector2.right;
			}
		}
		public void UpdateFacing(float moveHorizontal)
		{
			bool faceLeft = moveHorizontal < 0f;
			bool faceRight = moveHorizontal > 0f;

			print (faceLeft);
			print("faceleft");
			print (faceRight);
			print ("faceright");
			print (moveHorizontal);
			print ("move horizontal");

			print (m_SpriteForward);
			print ("forward");
			if (faceLeft && (m_SpriteForward.x == 1))
			{
				SetFacingData(-1);
			}
			else if (faceRight && (m_SpriteForward.x == -1))
			{
				SetFacingData(1);
			}
		}

		public void SetHorizontalSpeed(float horizontalSpeed)
		{
			m_Animator.SetFloat ("Speed", horizontalSpeed);
			m_MoveVector.x = horizontalSpeed * speed;
		}


	}



}
 */
